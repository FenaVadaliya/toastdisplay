package com.aswdc.toastdisplay;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {

    EditText etFirstName, etLastName;
    Button btnSubmit;
    ImageView ivBackground;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        etFirstName = findViewById(R.id.etActFirstName);
        etLastName = findViewById(R.id.etActLastName);
        btnSubmit = findViewById(R.id.btnActSubmit);
        ivBackground = findViewById(R.id.ivActBackground);


        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String text = etFirstName.getText().toString();
                String text1 = etLastName.getText().toString();

                Toast.makeText(getApplicationContext(), " " + text + "  " + text1,
                        Toast.LENGTH_SHORT).show();

            }
        });

    }
}